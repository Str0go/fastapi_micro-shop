from sqlalchemy import select
from sqlalchemy.engine import Result
from sqlalchemy.ext.asyncio import AsyncSession

from core.models import Product
from .schemas import ProductCreate, ProductUpdate, ProductUpdatePartial


async def get_products(session: AsyncSession) -> list[Product]:
    statement = select(Product).order_by(Product.id)
    result: Result = await session.execute(statement)
    products = result.scalars().all()
    return list(products)


async def get_product(product_id: int, session: AsyncSession) -> Product | None:
    return await session.get(Product, product_id)


async def create_product(product_in: ProductCreate, session: AsyncSession) -> Product:
    product = Product(**product_in.model_dump())
    session.add(product)
    await session.commit()
    # await session.refresh(product)
    return product


async def update_product(
        product: Product,
        product_update: ProductUpdate | ProductUpdatePartial,
        session: AsyncSession,
        partial: bool = False,
) -> Product:
    for name, value in product_update.model_dump(exclude_unset=partial).items():
        setattr(product, name, value)
    await session.commit()
    return product


async def delete_product(
        product: Product,
        session: AsyncSession,
) -> None:
    await session.delete(product)
    await session.commit()




