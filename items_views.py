from fastapi import APIRouter, Path
from typing_extensions import Annotated

router = APIRouter(prefix="/items")


@router.get("/")
def list_items():
    return ["one", "two"]


@router.get("/latest/")
def list_items():
    return {"it": {"id": 'latest'}}


@router.get("/{item_id}/")
def list_items(item_id: Annotated[int, Path(ge=1, lt=1_000_000)]):
    return {"it": {"id": item_id + 1_0}}